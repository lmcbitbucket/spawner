game.waves = (function() {

    var waves = [];
    
    
   waves[0] = {
        name: "Wave 1: Easy Mode.",
        enemySpawnTime: convertSecondsToMilliseconds(9),
        enemySpeed: 120,
        spawners: [
                    {
                        top:    [1,1,1,0,0,0,0,1,1,1],
                        bottom: [0,0,0,0,0,0,0,0,0,0]
                    },
                    {
                        top:    [0,0,0,0,0,0,0,0,0,0],
                        bottom: [0,0,0,1,1,1,1,0,0,0]
                    },
                    {
                        top:    [0,0,0,0,0,0,0,0,0,0],
                        bottom: [1,1,1,1,1,1,1,1,1,1]
                    },
                  ]
    }; 

    waves[1] = {
        name: "Wave 2: Walls and Walls of (slightly faster) Aliens",
        enemySpawnTime: convertSecondsToMilliseconds(8),
        enemySpeed: 140,
        spawners: [
                    {
                        top:    [1,1,1,1,1,1,1,1,1,1],
                        bottom: [0,0,0,0,0,0,0,0,0,0]
                    },
                    {
                        top:    [0,0,0,0,0,0,0,0,0,0],
                        bottom: [0,0,1,1,1,1,1,1,0,0]
                    },
                    {
                        top:    [0,0,1,1,1,1,1,1,0,0],
                        bottom: [0,0,0,0,0,0,0,0,0,0]
                    },
                    {
                        top:    [0,0,0,0,0,0,0,0,0,0],
                        bottom: [1,1,1,1,1,1,1,1,1,1]
                    },
                  ]
    };
    
    waves[2] = {
        name: "Wave 3: Zig-Zag Aliens (Don't panic, there's a safe zone)",
        enemySpawnTime: convertSecondsToMilliseconds(1),
        enemySpeed: 180,
        spawners: [
                    {
                        top:    [1,0,0,0,0,0,0,0,0,0],
                        bottom: [0,1,0,0,0,0,0,0,0,0]
                    },
                    {
                        top:    [1,0,1,0,0,0,0,0,0,0],
                        bottom: [0,1,0,1,0,0,0,0,0,0]
                    },
                    {
                        top:    [1,0,1,0,1,0,0,0,0,0],
                        bottom: [0,1,0,1,0,1,0,0,0,0]
                    },
                    {
                        top:    [1,0,1,0,1,0,1,0,0,0],
                        bottom: [0,1,0,1,0,1,0,1,0,0]
                    },
                    {
                        top:    [0,0,0,0,0,0,0,0,1,0],
                        bottom: [0,0,0,0,0,0,0,0,0,1]
                    },
                  ]
    };
    
    
    waves[3] = {
        name: "Wave 4: Let's get serious",
        enemySpawnTime: convertSecondsToMilliseconds(7),
        enemySpeed: 180,
        spawners: [
                    {
                        top:    [0,0,0,0,0,0,0,0,0,0],
                        bottom: [1,1,1,1,0,0,1,1,1,1]
                    },
                    {
                        top:    [1,1,0,1,1,1,0,1,1,0],
                        bottom: [0,0,0,0,0,0,0,0,0,0]
                    },
                    {
                        top:    [0,0,1,0,0,0,1,0,1,0],
                        bottom: [1,0,0,0,1,0,0,1,0,0]
                    },
                    {
                        top:    [1,0,1,0,0,0,1,0,1,0],
                        bottom: [0,1,0,0,0,1,0,0,0,1]
                    },
                    {
                        top:    [0,0,0,0,0,0,0,0,0,0],
                        bottom: [1,1,1,1,0,0,1,1,1,1]
                    },
                  ]
    };
/*    
 */   
    
    waves[4] = {
        name: "Wave 5: pwner",
        enemySpawnTime: convertSecondsToMilliseconds(9),
        enemySpeed: 160,
        spawners: [
                    {
                        top:    [1,1,1,1,1,1,1,1,1,1],
                        bottom: [1,1,1,1,1,1,1,1,1,1]
                    },
                    {
                        top:    [1,0,1,0,1,0,1,0,1,0],
                        bottom: [0,1,0,1,0,1,0,1,0,1]
                    },
                    {
                        top:    [1,1,1,0,0,0,0,1,1,1],
                        bottom: [1,1,1,0,0,0,0,1,1,1]
                    },
                    {
                        top:    [0,0,1,1,1,0,0,0,0,0],
                        bottom: [0,0,0,0,0,1,1,1,0,0]
                    },
                    {
                        top:    [0,1,1,0,1,0,1,0,1,0],
                        bottom: [1,0,0,1,0,1,0,1,0,1]
                    },
                    {
                        top:    [0,0,0,0,0,0,0,0,0,0],
                        bottom: [0,0,0,0,0,0,0,0,0,0]
                    },
                    {
                        top:    [1,0,0,0,1,1,1,1,1,0],
                        bottom: [0,1,1,1,1,1,0,0,0,1]
                    },
                  ]
    };
    
    
    function convertSecondsToMilliseconds(time){
        return time * 1000;
    }
    
    
    function convertMinutesToMilliseconds(time){
        return time * 60 * 1000;
    }
    
    return{
        waves: waves
    };

})();
