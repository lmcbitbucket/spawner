game.collisions = (function(){
    var _missiles;
    var _player;
    var _enemies;
    
    var _onPlayerHit;
    var _onEnemyHit;
    var _onEnemiesHitEachOther;
    
    
    function update(){
        checkMissileToEnemyCollisions();
        checkPlayerToEnemyCollisions();
        checkEnemiesHitEachOther();
    }
    
    function checkMissileToEnemyCollisions(){
        for(var i = 0; i < _missiles.length; i++){
            var missile = _missiles[i];
            if(missile.active)
                checkMissileHitEnemies(missile);
        }
    }
    

    function checkMissileHitEnemies(missile){
        for(var i = 0; i < _enemies.length; i++){
            var enemy = _enemies[i];
            if(enemy.active && doRectanglesCollide(missile,enemy)){
                _onMissileHit(missile);
                _onEnemyHit(enemy, missile);
            }
        }
    }
    

    function checkPlayerToEnemyCollisions(){
        if(!_player.active)
            return;

        for(var i = 0; i < _enemies.length; i++){
            var enemy = _enemies[i];
            if(enemy.active && doRectanglesCollide(_player,enemy))
                _onPlayerHit();
        }
    }
    
    function checkEnemiesHitEachOther(){
        for(var i = 0; i < _enemies.length; i++){
            var enemy = _enemies[i];
            if(enemy.active){
                for(var j = i+1; j < _enemies.length; j++){
                    var secondEnemy = _enemies[j];
                    if(secondEnemy.active && doRectanglesCollide(enemy,secondEnemy))
                        _onEnemiesHitEachOther(enemy,secondEnemy);
                }
            }
        }
    }
    
    
    function doRectanglesCollide(rect1, rect2){
        var xVector = Math.abs(rect1.getCenterX() - rect2.getCenterX());
        var yVector = Math.abs(rect1.getCenterY() - rect2.getCenterY());
        
        var combinedHalfWidth = rect1.getHalfWidth() + rect2.getHalfWidth();
        var combinedHalfHeight = rect1.getHalfHeight() + rect2.getHalfHeight();
        
        return combinedHalfWidth > xVector &&
               combinedHalfHeight > yVector;
    }
    

    return {
        setMissileCollection : function(missiles, onMissileHit){
            _missiles = missiles;
            _onMissileHit = onMissileHit;
        },
        
        setPlayer : function(player, onPlayerHit){
            _player = player;
            _onPlayerHit = onPlayerHit;
        },
        
        setEnemyCollection : function(enemies, onEnemyHit, onEnemyHitEnemy){
            _enemies = enemies;
            _onEnemyHit = onEnemyHit;
            _onEnemiesHitEachOther = onEnemyHitEnemy;
        },

        update : update
    };
})();
