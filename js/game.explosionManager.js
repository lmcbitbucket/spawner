game.explosions = (function() {

    var _explosionTileSize = 32;
    //both height and width

    var _explosionPool = [];
    var _activeExplosions = [];
    
    var _explosionSound;

    function initialize() {
        for (var i = 0; i < 20; i++) {
            _explosionPool.push(game.explosion.create());
        }
        
        _explosionSound = game.assets.sounds["Explosion"];
    }

    function triggerExplosion(point) {
        var x = centerExplosionPoint(point.x);
        var y = centerExplosionPoint(point.y);
        var explosion = _explosionPool.shift();
        explosion.getReady(x, y);
        _activeExplosions.push(explosion);
        _explosionPool.push(explosion);
        
        game.playSound(_explosionSound);
    }

    function centerExplosionPoint(coordinatePart) {
        return coordinatePart - _explosionTileSize / 2;
    }

    function update() {
        for (var i = 0; i < _activeExplosions.length; i++){
            _activeExplosions[i].update();
            if(!_activeExplosions[i].active){
                _activeExplosions.splice(i,1);
                i--;
            }
        }
    }

    function draw(display) {
        for (var i = 0; i < _activeExplosions.length; i++) {
            if (_activeExplosions[i].active) {
                _activeExplosions[i].draw(display);
            }
        }
    }

    return {
        initialize : initialize,
        triggerExplosion : triggerExplosion,
        update : update,
        draw : draw
    };

})();
