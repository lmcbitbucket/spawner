game.run = function() {
    var _canvas = document.querySelector("#gameCanvas");
    var _display = _canvas.getContext("2d");

    var _player;
    var _missileManager;
    var _respawner;
    var _enemyFactory;
    var _waves;
    var _waveManager;
    var _collisionDetector;
    var _explosions;

    var _fps = 0;
    var _lastDrawTime = 0;
    var _lastFpsReportTime = 0;
    
    var _hud;

    var Keys = {
        Left : 37,
        Up : 38,
        Right : 39,
        Space : 32,
        P: 80
    };

    var _playerActions = {
        Left : false,
        Right : false,
        Shoot : false,
    };

    function initialize() {
        game.setGameMessage("Loading ...");
        game.assets.beginLoadAssets(onAssetsLoaded);
    }
    

    function onAssetsLoaded() {
        game.currentState = game.gameStates.Loaded;

        window.addEventListener("keydown", handleKeyDown, false);
        window.addEventListener("keyup", handleKeyUp, false);

        _collisionDetector = game.collisions;
        _missileManager = game.missiles.initialize();

        _explosions = game.explosions;
        _explosions.initialize();

        _respawner = game.respawner.initialize();
        _player = game.player.initialize(_playerActions, _respawner, _missileManager);
        _waves = game.waves;
        _waveManager = game.waveManager;
        _enemyFactory = game.enemy;
        _hud = game.hud;
        _waveManager.initialize(_waves.waves, _enemyFactory, _explosions, _player);
        game.setGameMessage("Spawner is ready.  Press P to start game.");
    }

    function handleKeyDown(event) {
        var key = event.keyCode;

        if (key === Keys.Left){
            _playerActions.Left = true;
            event.preventDefault();

        }
        if (key === Keys.Right){
            _playerActions.Right = true;
            event.preventDefault();

        }
        if (key === Keys.Space){
            _playerActions.Shoot = true;
            event.preventDefault();
        }

        if (key === Keys.Up){
            _player.changeWeapon();
            event.preventDefault();
        }

        if(key === Keys.P){
            onPlayKeyPressed();
            event.preventDefault();
        }
    }

    function handleKeyUp(event) {
        var key = event.keyCode;

        if (key === Keys.Left)
            _playerActions.Left = false;
        if (key === Keys.Right)
            _playerActions.Right = false;
        if (key === Keys.Space)
            _playerActions.Shoot = false;

        event.preventDefault();
    }

    function playGame() {
        drawGame();
        updateGame();
        requestAnimationFrame(playGame, _canvas);
    }

    function drawGame() {
        clearScreen();
        calculateFps();
        _lastDrawTime = Date.now();

        if (isGamePlaying()) {
            _player.draw(_display);
            _missileManager.draw(_display);
            _waveManager.draw(_display);
            _respawner.draw(_display);
            _explosions.draw(_display);
        }
        if(isGamePlaying() || isGameTransitioningWaves()){
           _hud.draw(_display); 
        }
        
        if(!isGamePlaying())
            game.displayGameMessage(_display);
    }

    function calculateFps() {
        var now = Date.now();
        _fps = 1000 / (now - _lastDrawTime);
    }

    function clearScreen() {
        _display.fillStyle = "black";
        _display.fillRect(0, 0, _canvas.height, _canvas.width);
    }

    function updateGame() {
        if (isGamePlaying()) {
            game.collisions.update();
            _player.update(_fps);
            _missileManager.update(_fps);
            _respawner.update(_fps);
            _waveManager.update(_fps);
            _explosions.update();
        } 
        if(isGamePlaying() || isGameTransitioningWaves()){
            _hud.update(_player,_fps);
        }
    }
    
    function isGamePlaying(){
       return game.currentState == game.gameStates.Playing; 
    }
    
    function isGameOver(){
        return game.currentState == game.gameStates.Over;
    }
    
    function isGameLoaded(){
        return game.currentState == game.gameStates.Loaded;
    }
    
    function isGameTransitioningWaves(){
       return  game.currentState == game.gameStates.WaveTransition;
    }
    
    function onPlayKeyPressed(){
        if(isGameLoaded() || isGameOver()){
            _player.setStartGameState();
            _waveManager.setStartGameState();
            _waveManager.loadNextWave();
        }
    }
    

    initialize();
    playGame();
};