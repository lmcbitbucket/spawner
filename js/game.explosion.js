game.explosion = (function(){
    var _explosionTiles;
    
    var _explosionFrames = [  
                             {row:0, col: 0},
                             {row:1, col: 0},
                             {row:2, col: 0},
                             {row:3, col: 0},
                             {row:4, col: 0},
                             {row:5, col: 0},
                             {row:4, col: 0},
                             {row:5, col: 0},
                             {row:3, col: 0},
                             {row:2, col: 0},
                             {row:1, col: 0},
                             {row:0, col: 0},
                           ];
                           
     var _animationFrameDuration = 50;
     
     function create(){
         if(_explosionTiles == undefined)
            _explosionTiles = game.createTileSet(game.assets.images["Explosions"],0,0,32,32);
         //Hack:  Designed myself into a corner with this one. Relying on the closure to deactivate the explosion object from INSIDE one of its own dependencies (the animation).
         var animation = game.createAnimation(_explosionTiles,_explosionFrames,_animationFrameDuration,function(){explosion.active = false;});
         var behaviors = {'Explode':animation};
         var explosion = game.createSprite(behaviors,"Explode",0,0,0,0,32,32);
         explosion.active = false;
         
         explosion.getReady = function (x,y){
             explosion.position.x = x;
             explosion.position.y = y;
             animation.reset();
             explosion.active = true;
         };
         
         
         explosion.update = function(){
             if(this.active)
                this.updateAnimation();
         };
         
         
         return explosion;
     }
     
     return {
         create: create
     };
})();