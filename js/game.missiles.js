game.missiles = (function() {

    function Missile() {
        var missile = game.createMoveableObject(0, 0, 0, 0, 4, 4);
        missile.active = false;
        missile.getReady = function(x, y, velocityModifier) {
            missile.position.x = x;
            missile.position.y = y;
            missile.velocityModifier = velocityModifier;
            missile.active = true;
        };
        return missile;
    }

    var _missileManager;
    var _activeMissiles;
    var _missilePool;
    var _missileColor = "orange";
    var _missileVelocity = 900;

    var _shootSound;

    function initialize() {
        _missilePool = [];
        _activeMissiles = [];

        for (var i = 0; i < 20; i++)
            _missilePool.push(new Missile());

        _missileManager = this;
        appendMethodsToMissileManager();
        game.collisions.setMissileCollection(_activeMissiles, onMissileHit);
        _shootSound = game.assets.sounds["Missiles"];
    }

    function onMissileHit(missile) {
        missile.active = false;
    }

    function shoot(originX, originY, velocityModifier) {
        var missile = _missilePool.shift();
        missile.getReady(originX, originY, velocityModifier);
        _activeMissiles.push(missile);
        _missilePool.push(missile);
        //potential race condition if there is no delay between shots (delay is up to the player object at this point)
        game.playSound(_shootSound);
    }

    function update(fps) {
        updateActiveMissiles(fps);
        clearInactiveMissiles();
    }

    function updateActiveMissiles(fps) {

        for (var i = 0; i < _activeMissiles.length; i++) {
            var missile = _activeMissiles[i];
            missile.velocity.y = Math.floor(_missileVelocity/fps * missile.velocityModifier);
            missile.position.y += missile.velocity.y;
            if (missile.position.y < 0 || missile.position.y > game.worldDimensions.height)
                missile.active = false;
        }
    }

    function clearInactiveMissiles() {
        for (var i = 0; i < _activeMissiles.length; i++) {
            var missile = _activeMissiles[i];
            if (!missile.active) {
                _activeMissiles.splice(i, 1);
                i--;
            }
        }
    }

    function draw(display) {
        for (var i = 0; i < _activeMissiles.length; i++) {
            var missile = _activeMissiles[i];
            drawMissile(missile, display);
        }
    }

    function drawMissile(missile, display) {
        display.fillStyle = _missileColor;
        display.fillRect(missile.position.x, missile.position.y, missile.size.width, missile.size.height);
    }

    function appendMethodsToMissileManager() {
        _missileManager.shoot = shoot;
        _missileManager.update = update;
        _missileManager.draw = draw;

        _missileManager.deactivateAllMissiles = function(){
            for(var i = 0; i < _activeMissiles.length;i++){
                _activeMissiles[i].active = false;
            }
        };
    }

    return {
        initialize : function() {
            initialize();
            return _missileManager;
        }
    };
})();
