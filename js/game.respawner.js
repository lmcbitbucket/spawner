game.respawner = (function() {

    var _respawner;

    var _respawnerColor = "orange";

    var _sizeChangeTimer = 30;
    var _lastSizeChangeTime = 0;

    var _respawnerSpeed = 500;
    var _minRadius = 5;
    var _maxRadius = 15;
    var _currentRadius = 5;

    var _isExpanding = true;

    var _respawnerActivatedSound;
    var _respawnerShotSound;

    function initialize() {
        _respawner = game.createMoveableObject(0, 0, 0, 0, 0, 0);
        appendMethodsToRespawner();
        _respawnerActivatedSound = game.assets.sounds["SpawnerActivated"];
        _respawnerShotSound = game.assets.sounds["SpawnerShot"];
    }

    function shoot(xOrigin, yOrigin, velocityModifier) {
        if (!_respawner.active) {
            getReady(xOrigin, yOrigin,velocityModifier);
            game.playSound(_respawnerShotSound);
        } else 
        {
            if (_respawner.onActivated) {
                _respawner.onActivated(_respawner.position.x, _respawner.position.y);
                _respawner.active = false;
                game.playSound(_respawnerActivatedSound);
            }
        }
    }

    function getReady(xOrigin, yOrigin, modifier) {
        _respawner.position.x = xOrigin;
        _respawner.position.y = yOrigin;
        _respawner.velocity.y = _respawnerSpeed;
        _respawner.velocityModifier = modifier;
        _currentRadius = _minRadius;
        _isExpanding = true;
        _respawner.active = true;
    }

    function update(fps) {
        if (!_respawner.active)
            return;

        updateSize();
        updateLocation(fps);
    }

    function updateSize() {
        if (Date.now() - _lastSizeChangeTime < _sizeChangeTimer)
            return;

        if (_isExpanding)
            _currentRadius++;
        else
            _currentRadius--;

        if (_currentRadius > _maxRadius) {
            _currentRadius = _maxRadius;
            _isExpanding = false;
        }

        if (_currentRadius < _minRadius) {
            _currentRadius = _minRadius;
            _isExpanding = true;
        }

        _lastSizeChangeTime = Date.now();
    }

    function updateLocation(fps) {
        var trueVelocity = Math.floor((_respawnerSpeed/fps) * _respawner.velocityModifier);
        _respawner.velocity.y = trueVelocity;
        _respawner.position.y += _respawner.velocity.y;

        if (_respawner.position.y < 0 || _respawner.position.y > game.worldDimensions.height)
            _respawner.active = false;
    }

    function draw(display) {
        if (!_respawner.active)
            return;

        display.strokeStyle = _respawnerColor;
        display.beginPath();
        display.arc(_respawner.position.x, _respawner.position.y, _currentRadius, 0, 2 * Math.PI);
        display.stroke();
        
        display.fillStyle = _respawnerColor;
        display.beginPath();
        display.arc(_respawner.position.x, _respawner.position.y, _minRadius - 2, 0, 2 * Math.PI);
        display.fill();
    }

    function appendMethodsToRespawner() {
        _respawner.active = false;
        _respawner.update = update;
        _respawner.draw = draw;
        _respawner.shoot = shoot;
        _respawner.onActivated = undefined;
        _respawner.velocityModifier = 1;
        
        _respawner.deactivate = function(){
            _respawner.active = false;
        };
    }

    return {
        initialize : function() {
            initialize();
            return _respawner;
        }
    };
})();
