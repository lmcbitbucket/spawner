var game = (function() {

    function Point(x, y) {
        this.x = x;
        this.y = y;
    }

    function Size(height, width) {
        this.width = width;
        this.height = height;
    }


    Size.prototype.getHalfWidth = function() {
        return Math.floor(this.width / 2);
    };

    Size.prototype.getHalfHeight = function() {
        return Math.floor(this.height / 2);
    };

    function Positionable() {
        this.getHalfWidth = function() {
            return this.size.getHalfWidth();
        };

        this.getHalfHeight = function() {
            return this.size.getHalfHeight();
        };

        this.getCenterX = function() {
            return this.position.x + this.getHalfWidth();
        };

        this.getCenterY = function() {
            return this.position.y + this.getHalfHeight();
        };
    }

    function MoveableObject(x, y, vx, vy, height, width) {
        this.velocity = new Point(vx, vy);
        this.position = new Point(x, y);
        this.size = new Size(width, height);
        this.visible = true;
        this.rotation = 0;
    }


    MoveableObject.prototype = new Positionable();

    function TileSet(image, sourceX, sourceY, tileHeight, tileWidth) {
        this.height = tileHeight;
        this.width = tileWidth;

        this.getImage = function() {
            return image;
        };

        this.getFrameX = function(col) {
            return sourceX + (col * tileWidth);
        };

        this.getFrameY = function(row) {
            return sourceY + (row * tileHeight);
        };
    }

    function Animation(tileSet, frames, frameDuration, onAnimationFinished) {
        var animationTimer = 0;
        var currentFrame = 0;
        var frameCount = frames.length;
        var drawInfo = {
            image : tileSet.getImage(),
            frameX : undefined,
            frameY : undefined,
            width : tileSet.width,
            height : tileSet.height
        };

        function isNextFrameDue() {
            return Date.now() - animationTimer >= frameDuration;
        }

        function canFrameAdvance() {
            return currentFrame + 1 < frameCount;
        }

        function reset() {
            currentFrame = 0;
            animationTimer = Date.now();
        };

        this.getDrawInfo = function() {
            drawInfo.frameX = tileSet.getFrameX(frames[currentFrame].col);
            drawInfo.frameY = tileSet.getFrameY(frames[currentFrame].row);
            return drawInfo;
        };

        this.update = function() {
            if (animationTimer == 0)
                reset();

            if (isNextFrameDue()) {
                if (canFrameAdvance()) {
                    currentFrame++;
                    animationTimer = Date.now();
                } else {
                    if (onAnimationFinished)
                        onAnimationFinished();
                }
            }
        };

        this.reset = reset;
    }

    function Sprite(behaviors, initialBehavior, x, y, vx, vy, height, width) {

        var _behaviors = behaviors;
        var _behavior = initialBehavior;

        this.size = new Size(height, width);
        this.position = new Point(x, y);
        this.velocity = new Point(vx, vy);
        this.visible = true;
        this.rotation = 0;

        function getCurrentAnimation() {
            return _behaviors[_behavior];
        }


        this.getCurrentBehavior = function() {
            return behavior;
        };

        this.setCurrentBehavior = function(behaviorName) {
            if (_behaviors[behaviorName])
                _behavior = behaviorName;
            else
                throw "Behavior does not exist.";
        };

        this.updateAnimation = function() {
            getCurrentAnimation().update();
        };

        this.draw = function(display) {
            if (!this.visible)
                return;

            display.save();
            display.translate(this.getCenterX(), this.getCenterY());
            display.rotate(this.rotation * Math.PI / 180);

            var drawInfo = getCurrentAnimation().getDrawInfo();
            display.drawImage(drawInfo.image, drawInfo.frameX, drawInfo.frameY, drawInfo.width, drawInfo.height, -this.getHalfWidth(), -this.getHalfHeight(), this.size.width, this.size.height);

            display.restore();
        };
    }


    Sprite.prototype = new Positionable();

    var _gameWorldDimensions = new Size(640, 640);

    function getWorldCenterX() {
        return Math.floor(_gameWorldDimensions.width / 2);
    }

    function getWorldCenterY() {
        return Math.floor(_gameWorldDimensions.height / 2);
    }

    function playSound(audio) {
        audio.play();
        audio.currentTime = 0;
    }

    var _gameStates = {
        Loading : 0,
        Loaded: 1,
        Playing : 2,
        WaveTransition : 3,
        Over : 4,
    };

    var _gameMessage;
    function setGameMessage(message) {
        _gameMessage = message;
    }

    function displayGameMessage(display) {
        display.fillStyle = 'white';
        display.font = '12px "c64"';
        display.textAlign = 'center';
        display.fillText(_gameMessage, getWorldCenterX(), getWorldCenterY());
    }

    var _currentState = _gameStates.Loading;

    return {
        createTileSet : function(image, sourceX, sourceY, tileHeight, tileWidth) {
            return new TileSet(image, sourceX, sourceY, tileHeight, tileWidth);
        },

        createAnimation : function(tileSet, frames, frameDuration, onAnimationFinished) {
            return new Animation(tileSet, frames, frameDuration, onAnimationFinished);
        },

        createSprite : function(behaviors, initialBehavior, x, y, vx, vy, height, width) {
            return new Sprite(behaviors, initialBehavior, x, y, vx, vy, height, width);
        },

        createMoveableObject : function(x, y, vx, vy, height, width) {
            return new MoveableObject(x, y, vx, vy, height, width);
        },

        playSound : playSound,
        worldDimensions : _gameWorldDimensions,
        gameStates : _gameStates,
        currentGameState : _currentState,
        setGameMessage : setGameMessage,
        displayGameMessage : displayGameMessage,
    };
})();
