game.waveManager = (function() {

    var _waves;
    var _waveCount;

    var _currentWave;
    var _currentWaveIndex;

    var _waveSpawnCount;
    var _currentSpawnIndex;

    var _waveEnemyCount;
    var _enemiesKilled;

    var _spawnTimer = 0;
    var _lastEnemySpawnTime = 0;
    var _enemySpeed = 0;
    var _velocityPenalty = 3;

    var _explosionManager;
    var _player;

    var _enemyPool = [];
    var _activeEnemies = [];

    function initialize(waves, enemyFactory, explosionManager, player) {
        _waves = waves;
        _enemyFactory = enemyFactory;
        _explosionManager = explosionManager;
        _player = player;

        game.collisions.setEnemyCollection(_activeEnemies, onEnemyHit, onEnemyHitEnemy);
        game.collisions.setPlayer(_player, onPlayerHit);
        _waveCount = _waves.length;
    }
    
    function setStartGameState(){
        _currentWaveIndex = 0;
    }

    function loadNextWave() {
        if (_currentWaveIndex < _waveCount) {

            clearEnemyArrays();

            _currentWave = _waves[_currentWaveIndex];

            game.setGameMessage(_currentWave.name);
            game.currentState = game.gameStates.WaveTransition;

            _player.active = false;
            _player.visible = false;

            _waveSpawnCount = _currentWave.spawners.length;
            _currentSpawnIndex = -1;

            _waveEnemyCount = getEnemyCount(_currentWave);
            _enemiesKilled = 0;

            _enemySpeed = _currentWave.enemySpeed;
            _spawnTimer = _currentWave.enemySpawnTime;

            window.setTimeout(function() {
                createWaveEnemies();
                _player.active = true;
                _player.visible = true;
                _player.prepareForWave();
                game.currentState = game.gameStates.Playing;
                game.setGameMessage("");
            }, 3000);

        } else {
            game.setGameMessage("You win! Press P to play again.");
            game.currentState = game.gameStates.Over;
        }
    }

    function clearEnemyArrays() {
        _enemyPool.length = 0;
        _activeEnemies.length = 0;
    }

    function getEnemyCount(wave) {
        var count = 0;
        for (var i = 0; i < wave.spawners.length; i++) {
            var spawn = wave.spawners[i];
            count += getSpawnEnemyCount(spawn.top) + getSpawnEnemyCount(spawn.bottom);
        }
        return count;
    }

    function getSpawnEnemyCount(spawn) {
        var count = 0;
        for (var i = 0; i < spawn.length; i++) {
            if (spawn[i] == 1)
                count++;
        }
        return count;
    }

    function createWaveEnemies() {
        for (var i = 0; i < _waveEnemyCount; i++) {
            (function() {
                var enemy = _enemyFactory.create(_enemySpeed);
                _enemyPool.push(enemy);
            })();
        }
    }

    var _isNextWaveLoading = false;
    function update(fps) {
        if (canWaveSpawnEnemies() && areWaveEnemiesDue()) {
            _currentSpawnIndex++;
            spawnNextEnemyGroup();
        }
        for (var i = 0; i < _activeEnemies.length; i++) {
            if (_activeEnemies[i].active)
                _activeEnemies[i].update(fps);
        }

        if (isWaveComplete() && !_isNextWaveLoading) {
            _isNextWaveLoading = true;
            _currentWaveIndex++;

            setTimeout(function() {
                loadNextWave();
                _isNextWaveLoading = false;
            }, 1000);
        }
    }

    function canWaveSpawnEnemies() {
        return _currentSpawnIndex + 1 < _waveSpawnCount;
    }

    function areWaveEnemiesDue() {
        return Date.now() - _lastEnemySpawnTime > _spawnTimer;
    }

    function spawnNextEnemyGroup() {
        var spawn = _currentWave.spawners[_currentSpawnIndex];
        //top enemy spawns
        for (var i = 0; i < spawn.top.length; i++) {
            if (spawn.top[i] == 1) {
                (function() {
                    var enemy = _enemyPool.shift();
                    var x = i * enemy.size.width;
                    var y = 0;
                    enemy.getReady(x, y, 1);
                    _activeEnemies.push(enemy);
                })();
            }
        }

        //bottom enemy spawns
        for (var j = 0; j < spawn.bottom.length; j++) {
            if (spawn.bottom[j] == 1) {
                (function() {
                    var enemy = _enemyPool.shift();
                    var x = j * enemy.size.width;
                    var y = game.worldDimensions.height - enemy.size.height;
                    enemy.getReady(x, y, -1);
                    enemy.rotation = 180;
                    _activeEnemies.push(enemy);
                })();
            }
        }
        _lastEnemySpawnTime = Date.now();
    }

    function isWaveComplete() {
        return _enemiesKilled == _waveEnemyCount;
    }

    function restartWave() {
        loadNextWave();
    }

    function draw(display) {
        for (var i = 0; i < _activeEnemies.length; i++) {
            if (_activeEnemies[i].active)
                _activeEnemies[i].draw(display);
        }
    }

    //Collision Handlers
    function onEnemyHit(enemy, missile) {
        if (isMissileHittingSquishyPart(enemy, missile)) {
            _explosionManager.triggerExplosion({
                x : enemy.getCenterX(),
                y : enemy.getCenterY()
            });
            enemy.active = false;
            _enemiesKilled++;
        } else {
            var knockBack = 6;//Math.floor(enemy.size.height * .10);  Enemy height is always 64 unless I change the tile.. avoid the calculation
            if (enemy.isMovingDown()) {
                enemy.position.y -= knockBack;
            } else {
                enemy.position.y += knockBack;
            }
        }
    }

    function isMissileHittingSquishyPart(enemy, missile) {
        return enemy.velocity.y > 0 && missile.velocity.y > 0 || (enemy.velocity.y < 0 && missile.velocity.y < 0);
    }

    function onEnemyHitEnemy(enemy1, enemy2) {
        if (enemy1.position.x != enemy2.position.x)
            return;
        if (enemy1.rotation != enemy2.rotation)
            return;

        if (enemy1.position.y > enemy2.position.y)
            enemy1.position.y = enemy2.position.y - (enemy2.size.height + 1);

        if (enemy1.position.y < enemy2.position.y)
            enemy2.position.y = enemy1.position.y + (enemy1.size.height + 1);
    }

    function onPlayerHit() {
        if(!_player.active)
            return;

        _player.lives--;
        _explosionManager.triggerExplosion({
            x : _player.getCenterX(),
            y : _player.getCenterY()
        });
        _player.visible = false;
        _player.active = false;
        if (_player.lives == 0) {
            window.setTimeout(function() {
                game.setGameMessage("Game Over.  Press P to play again.");
                game.currentState = game.gameStates.Over;
            }, 2000);
        } else {
            setTimeout(function() {
                _player.active = true;
                _player.visible = true;
                _player.prepareForWave();
                restartWave();
            }, 750);
        }
    }

    return {
        initialize : initialize,
        loadNextWave : loadNextWave,
        restartWave : restartWave,
        update : update,
        draw : draw,
        setStartGameState: setStartGameState
    };
})();
