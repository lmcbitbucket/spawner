game.hud = (function() {

    var _weaponName;
    var _lives;
    var _isUsingRespawner = false;
    var _fps = 0;
    var _lastFpsReportTime = 0;
    

    function update(player, fps) {
        _lives = player.lives;
        _isUsingRespawner = player.isUsingRespawner();
        if(Date.now() - _lastFpsReportTime > 1000){
            _fps = Math.floor(fps);
            _lastFpsReportTime = Date.now();
        }
    }

    function draw(display) {
        drawLives(display);
        drawWeapon(display);
        drawFps(display);
    }

    function drawLives(display) {
        var y = game.worldDimensions.height - 25;
        var lifeBarHeight = 15;
        var lifeBarWidth = 5;

        for (var i = 1; i <= 2 * _lives; i += 2) {
            var x = i * 10;
            display.fillStyle = 'white';
            display.fillRect(x, y, lifeBarWidth, lifeBarHeight);
        }
    }

    function drawWeapon(display) {
        if (!_isUsingRespawner)
            return;

        var iconX = game.worldDimensions.width - 15;
        var iconY = game.worldDimensions.height - 15;
        var innerRadius = 3;
        var outerRadius = 8;

        display.beginPath();
        display.fillStyle = 'white';
        display.arc(iconX, iconY, innerRadius, 0, 2 * Math.PI);
        display.fill();
        display.strokeStyle = 'white';
        display.arc(iconX, iconY, outerRadius, 0, 2 * Math.PI);
        display.stroke();
    }
    
    function drawFps(display){
        var x = 5;
        var y = 15;
        
        display.fillStyle = 'white';
        display.font = '10px "c64"';
        display.textAlign = 'left';
        display.fillText(_fps + " FPS",x,y);
    }

    return {
        update : update,
        draw : draw,
    };
})(); 