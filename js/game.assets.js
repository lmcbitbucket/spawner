game.assets = (function(){
    
    var _assetCount = 0;
    var _assetsLoaded = 0;

    var _onAssetsLoaded;
    
    var _imageAssets = {
        Explosions : undefined,
        Player : undefined,
        Enemy : undefined
    };
    
    var _soundAssets = {
        Missiles : undefined,
        SpawnerShot : undefined,
        SpawnerActivated : undefined,
        Explosion : undefined 
    };
    
    
    function beginLoadAssets(onAssetsLoaded){
        if(!onAssetsLoaded)
            throw "Null or undefined value for onAssetsLoaded";
        
        _assetCount = Object.keys(_imageAssets).length + Object.keys(_soundAssets).length;
        loadImages();
        loadSounds();
        _onAssetsLoaded = onAssetsLoaded;
    }
    
    
    function loadImages(){
        Object.keys(_imageAssets).forEach(function(key){
            _imageAssets[key] = new Image();
            _imageAssets[key].addEventListener("load",countAssetAsLoaded);
            });

        _imageAssets["Explosions"].src = "resources/explosion.png";
        _imageAssets["Player"].src = "resources/player.png";
        _imageAssets["Enemy"].src = "resources/enemy.png";
    }
    
    function loadSounds(){
        Object.keys(_soundAssets).forEach(function(key){
            _soundAssets[key] = new Audio();
            _soundAssets[key].addEventListener("canplay",countAssetAsLoaded);
        });
            
        _soundAssets["Missiles"].src = "resources/missileshot.wav";
        _soundAssets["SpawnerShot"].src = "resources/spawnershot.wav";
        _soundAssets["SpawnerActivated"].src = "resources/spawneractivated.wav";
        _soundAssets["Explosion"].src = "resources/explosion.wav";
    }

    function countAssetAsLoaded(){
        _assetsLoaded++;
        if(_assetsLoaded == _assetCount)
            _onAssetsLoaded();
    }
    
    return{
        beginLoadAssets: beginLoadAssets,
        images: _imageAssets,
        sounds: _soundAssets
    };

})();
