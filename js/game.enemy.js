game.enemy = (function(){
    
    var _enemyTileSet = null;
    
    var _enemyFrames = [
                           {row:0, col:0},
                           {row:0, col:1},
                           {row:0, col:2},
                           {row:0, col:1},
                       ];
    
    var _animationFrameDuration = 150;
    var _enemyFriction = .80;
    
    function create(maxVelocity){
        if(!_enemyTileSet){
            _enemyTileSet = game.createTileSet(game.assets.images["Enemy"],0,0,64,64);
        }

        var enemyMoveAnimation = game.createAnimation(_enemyTileSet,_enemyFrames,_animationFrameDuration,function(){enemyMoveAnimation.reset();});
        var enemyBehaviors = {'Move' : enemyMoveAnimation};
        var enemy = game.createSprite(enemyBehaviors,"Move",0,0,0,0,64,64);
        enemy.rotation = 0;
        enemy.active = false;
        enemy.maxVelocity = maxVelocity;
        enemy.velocity.y = maxVelocity;
        
        appendEnemyMethods(enemy);
        return enemy;
    }
    
    
    function appendEnemyMethods(enemy){
        addGetReady(enemy);
        addUpdate(enemy);

        enemy.isMovingUp = function(){
            return this.rotation == 180;
        };
        
        enemy.isMovingDown = function(){
            return this.rotation == 0;
        };
    }
    
    function addGetReady(enemy){
        enemy.getReady = function(x,y,velModifier){
            enemy.position.x = x;
            enemy.position.y = y;
            enemy.velocity.y *= velModifier;
            enemy.active = true;
        };
    }
    
    function addUpdate(enemy){
        enemy.update = function(fps){
            if(!this.active)
                return;
                
            var trueVelocity = Math.floor(this.maxVelocity/fps);
                
            if(this.isMovingDown())
                this.velocity.y = trueVelocity;
            
            if(this.isMovingUp())
                this.velocity.y = -trueVelocity;

            this.position.y += this.velocity.y;
            
            if(this.position.y > game.worldDimensions.height){
                this.position.y = game.worldDimensions.height;
                rotateEnemy(enemy);
                changeEnemyDirection(enemy);
            }
            if(this.position.y < 0 - this.size.height){
                this.position.y = 0 - this.size.height + 1;
                rotateEnemy(enemy);
                changeEnemyDirection(enemy);
            }
            
            this.updateAnimation();
        };
    }
    

    function rotateEnemy(enemy){
        enemy.rotation = enemy.rotation == 0? 180: 0;
    }
    

    function changeEnemyDirection(enemy){

       enemy.velocity.y *= -1; 
    }
    

    return {
        create: create
    };

})();