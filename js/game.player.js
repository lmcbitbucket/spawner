game.player = (function() {

    var _playerSpeed = 250;

    var _moveAnimation;
    var _behaviors;
    var _player;

    var _playerActions;
    var _respawnerWeapon = false;

    var _missileManager;
    var _missileShootDelay = 150;
    var _lastMissileShotTime = 0;

    var _respawner;
    var _respawnerShootDelay = 350;
    var _lastRespawnerShotTime = 0;

    function initialize(playerActions, respawner, missileManager) {
        _playerActions = playerActions;
        _respawner = respawner;
        _missileManager = missileManager;
        var tiles = game.createTileSet(game.assets.images["Player"], 0, 0, 32, 32);

        var moveAnimationFrames = [{
            row : 0,
            col : 0
        }, {
            row : 0,
            col : 1
        }, {
            row : 0,
            col : 2
        }, {
            row : 0,
            col : 1
        }];

        _moveAnimation = game.createAnimation(tiles, moveAnimationFrames, 150, function() {
            _moveAnimation.reset();
        });
        _behaviors = {
            'Fly' : _moveAnimation
        };
        _player = game.createSprite(_behaviors, "Fly", 0, 0, 0, 0, 32, 32);
        _respawner.onActivated = respawnPlayer;

        appendMethodsToPlayer();
        return _player;
    }

    function centerOnScreen() {
        var worldCenterX = Math.floor((game.worldDimensions.width / 2) - _player.getHalfWidth());
        _player.position.x = worldCenterX;
        _player.position.y = Math.floor((game.worldDimensions.height / 2)) - (_player.size.height);
        _player.rotation = 180;
    }

    function update(fps) {
        if (!_player.active)
            return;

        updatePlayerVelocity(fps);
        movePlayer();
        shootWeapon();
        _player.updateAnimation();
    }

    function updatePlayerVelocity(fps) {

        var trueSpeed = Math.floor(_playerSpeed / fps);

        if (_playerActions.Left)
            _player.velocity.x = -trueSpeed;
        if (_playerActions.Right)
            _player.velocity.x = trueSpeed;

        if (!_playerActions.Left && !_playerActions.Right)
            _player.velocity.x = 0;
    }

    function movePlayer() {
        _player.position.x += _player.velocity.x;
        keepPlayerInBounds();
    }

    function keepPlayerInBounds() {
        if (_player.position.x < 0)
            _player.position.x = 0;
        if (_player.position.x > game.worldDimensions.width - _player.size.width)
            _player.position.x = game.worldDimensions.width - _player.size.width;

        if (_player.position.y < 0)
            _player.position.y = 0;
        if (_player.position.y > game.worldDimensions.height - _player.size.height)
            _player.position.y = game.worldDimensions.height - (_player.size.height);
    }

    function changeWeapon() {
        _respawnerWeapon = !_respawnerWeapon;
    }

    function shootWeapon() {
        if (!_playerActions.Shoot)
            return;

        var yOrigin = _player.rotation == 0 ? _player.position.y : _player.position.y + _player.size.height;
        var velocityModifier = _player.rotation == 0 ? -1 : 1;

        if (_respawnerWeapon)
            shootRespawner(yOrigin, velocityModifier);
        else
            shootMissile(yOrigin, velocityModifier);
    }

    function shootMissile(yOrigin, velocityModifier) {
        if (Date.now() - _lastMissileShotTime < _missileShootDelay)
            return;

        _missileManager.shoot(_player.getCenterX(), yOrigin, velocityModifier);
        _lastMissileShotTime = Date.now();
    }

    function shootRespawner(yOrigin, velocityModifier) {
        if (Date.now() - _lastRespawnerShotTime < _respawnerShootDelay)
            return;

        _respawner.shoot(_player.getCenterX(), yOrigin, velocityModifier);
        _lastRespawnerShotTime = Date.now();
    }

    function respawnPlayer(x, y) {
        var xPos = x - _player.getHalfWidth();
        var yPos = y - _player.getHalfHeight();
        _player.position.x = xPos;
        _player.position.y = yPos;
        _player.rotation = _player.rotation == 0 ? 180 : 0;
        keepPlayerInBounds();
    }

    function prepareForWave() {
        centerOnScreen();
        _missileManager.deactivateAllMissiles();
        _respawner.deactivate();
        _respawnerWeapon = false;
    }

    function setStartGameState() {
        _player.lives = 5;
        _player.active = true;
        _respawnerWeapon = false;
        centerOnScreen();
    }

    function appendMethodsToPlayer() {
        _player.update = update;
        _player.changeWeapon = changeWeapon;
        _player.prepareForWave = prepareForWave;
        _player.setStartGameState = setStartGameState;

        _player.isUsingRespawner = function() {
            return _respawnerWeapon;
        };
    }

    return {
        initialize : initialize
    };
})();
